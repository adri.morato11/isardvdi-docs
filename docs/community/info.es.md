# Comunidad IsardVDI

Bienvenido en la comunidad IsardVDI! Esta página está diseñada para proporcionar recursos y apoyo a los usuarios y colaboradores de la plataforma IsardVDI.

## Empezando

Si sois nuevo a IsardVDI, aquí tenéis algunos recursos para ayudaros a empezar:

- [Documentación IsardVDI](https://docs.isardvdi.com/en/latest/): La documentación oficial de la IsardVDI, que incluye instrucciones de instalación, guías de usuario y documentación de desarrollador.
- [Repositorio GitLab IsardVDI](https://gitlab.com/isardvdi/isard): El repositorio de código fuente para IsardVDI, donde encontraréis los últimos lanzamientos, el código de contribución y los problemas de informe.
- Foro IsardVDI (en construcción): Un foro comunitario para usuarios y colaboradores de IsardVDI, donde podéis hacer preguntas, compartir ideas y obtener apoyo de la comunidad.

## Contribuyendo a IsardVDI

IsardVDI es un proyecto de código abierto, y acogemos con satisfacción las contribuciones de cualquier persona interesada a ayudar a mejorar la plataforma. Aquí hay algunas maneras de involucraros:

- [Incidencias del GitLab](https://gitlab.com/isardvdi/isard/-/issues): Si encontráis un error o tenéis una petición de funcionalidad, podéis informarlo sobre problemas de GitLab. Este también es un buen lugar para encontrar cuestiones que necesitan ayuda de la comunidad.
- [Peticiones de fusión de GitLab](https://gitlab.com/isardvdi/isard/-/merge_requests): Si queréis contribuir con el código a IsardVDI, podéis enviar una petición de fusión a GitLab. Antes de hacerlo, leéis las nuestras [directrices de colaboración](https://gitlab.com/isardvdi/isard/-/blob/master/contributing.md).
- Foro comunitario (en construcción): Si tenéis preguntas sobre como contribuir a la IsardVDI, o queréis discutir una posible contribución, podéis utilizar la categoría de desarrollo de nuestro foro para conectaros con otros colaboradores.

## Orientaciones de la comunidad

Queremos fomentar una comunidad acogedora y solidaria en torno a IsardVDI. A tal efecto, pedimos que todos los miembros de la comunidad cumplan las siguientes directrices:

- Sed respetuosos: Damos la bienvenida a la diversidad y la inclusividad de nuestra comunidad, y pedimos que todo el mundo se trate con respeto y amabilidad.
- Sed constructivos: cuando ofrecéis comentarios o críticas, por favor, hacedlo de manera constructiva y útil.

## Conclusión

Esperamos que encuentres esta página útil para empezar y contribuir a IsardVDI. Gracias para formar parte de nuestra comunidad!