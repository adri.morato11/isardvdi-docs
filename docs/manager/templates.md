# Templates

In this section you can see all the templates that have been created from all categories.

![Templates section](./domains.images/domains7.png)

If you click on the ![[+]](./domains.images/domains5.png) icon, you can see more information about the template.

* Hardware: as its name indicates, it is the hardware that has been assigned to that template

* Allows: indicates who has been given permission to use the template

* Storage: indicates the UUID of the disk associated and its use

* Media: indicates the media associtated with the desktop, if there is any

![Template details](./domains.images/domains8.png)

## Enable

Enabling a template allows all users with whom the template is shared to view and use it for creating desktops.

To enable or disable a template, simply click on the checkbox in the "Enabled" column of the table and confirm your selection in the notification. 

![Enabled checkboxes](./domains.images/domains53.png)

If you click on the ![Hide Disabled](./domains.images/domains49.png) button located in the top right corner, the templates that are not enabled (visible to the users it's shared with) will not be shown in the table. When clicking on the button ![View Disabled](./domains.images/domains50.png) again they will be displayed.

## Duplicate

This option allows you to duplicate in database the template while keeping the original template disk.

Template duplication can be performed multiple times, and is useful for various scenarios, such as:

- You want to share template within other categories. You can duplicate the template and assign it to another user in another category with separate sharing permissions for the new copy while maintaining the original version

Deleting a duplicated template will not affect the original template.

When duplicating a template, you can customize the new copy:

- Name of the new template
- Description of the new template
- User assigned as the new owner of the template
- Enable or disable visibility of the template to shared users
- Select users to share the template with

![Duplicate template modal](./domains.images/domains46.png)

## Delete

!!! danger "Action not reversible"

    Currently deleting a template and its desktops and derived templates cannot be undone.
    With the input of [recyclebin Trash](https://gitlab.com/isard/isardvdi/-/merge_requests/1692)
    they can be recovered for a predefined time.

When deleting a template, a modal window will appear displaying all desktops created from it and any templates duplicated. 

![Delete button](./domains.images/domains47.png)

!!! Warning Dependencies
    Deleting the template will result in the deletion of all associated domains and their disks.

![Delete tree modal](./domains.images/domains48.png)

However, if the template to delete is duplicated from another template, there is no need to delete them all unless the origin template is deleted as well.

## Template as Server

A template that is marked as *server* will make all desktops that are created servers as well. This means that the system will always start these desktops if it finds them stopped.

To be able to generate a server from a template you have to go to the "Administration" panel.

Press the button ![](./template_server.images/template_server1.png)

![](./template_server.images/template_server2.png)

Once there, go to "Templates" under the "Manager" section.

Press the button ![](./template_server.images/template_server3.png)

![](./template_server.images/template_server4.png)

Press the button ![](./template_server.images/template_server5.png) of the template that you want to convert into a server.

![](./template_server.images/template_server6.png)

Press the button ![](./template_server.images/template_server7.png)

![](./template_server.images/template_server8.png)

And a dialog window will appear. You have to check the box "Set it as server"

![](./template_server.images/template_server9.png)