# Mitjans

A l'apartat ![](media.ca.images/mitjans1.png){width="8%"} es veuran els mitjans creats per tu i els compartits amb tu.

![](media.ca.images/mitjans3.png){width="80%"}

![](media.ca.images/mitjans4.png){width="80%"}


## Pujar mitjà

Per a pujar un nou mitjà es prem el botó ![](media.ca.images/mitjans5.png). A aquest nou formulari s'haurà d'establir un **enllaç directe a la descàrrega** de la imatge a pujar (del cas contrari la descàrrega fallarà), un nou nom, i assignar-li si és una imatge ISO o un disc floppy.

![](media.ca.images/mitjans7.png){width="80%"}


## Compartir mitjà

Per a compartir un mitjà, a la vista **Els teus mitjans**, es selecciona el mitjà a compartir i es prem la seva icona ![](media.ca.images/mitjans8.png) on apareixerà una finestra on poder assignar els grups i/o usuaris destí.

![](media.ca.images/mitjans9.png){width="60%"}


## Crear nou escriptori a partir de mitjà

Per a generar un escriptori a partir d'un mitjà, a la vista **Els teus mitjans**, es selecciona el mitjà i es prem la seva icona ![](media.ca.images/mitjans10.png) on et redirigeix a una pàgina on emplenar el formulari amb les dades necessàries (mateix formulari de **[crear un nou escriptori](../../user/create_desktop.ca/#vista-principal)**), amb l'apartat nou **Seleccionar una plantilla**:

![](media.ca.images/mitjans12.png){width="80%"}

A aquest apartat **NO** es selecciona una plantilla d'Isard, sinó un sistema operatiu de base sobre el qual el disc de l'escriptori es pugui basar (drivers). Depenent del sistema operatiu d'instal·lació del mitjà, així es selecciona la plantilla base.


## Esborrar mitjà

Per a eliminar una iso, a la vista **Els teus mitjans**, es selecciona el mitjà a esborrar i es prem la seva icona ![](media.ca.images/mitjans13.png) i s'accepta o declina l'acció d'esborrat del mitjà.

![](media.ca.images/mitjans14.png)
