# Documentación api

Actualmente estamos evaluando *swagger* para la documentación.

Mientras tanto podéis revisar los extremos (*endpoints*) en el código [api views](https://gitlab.com/isard/isardvdi/-/tree/main/api/src/api/views) y los parámetros de POST/PUT en los esquemas [cerberus](https://gitlab.com/isard/isardvdi/-/tree/main/api/src/api/schemas).

Hay un ejemplo de acceso a API en [https://gitlab.com/isard/isardvdi/-/blob/main/api/src/scripts/test_one_endpoint.py](https://gitlab.com/isard/isardvdi/-/blob/main/api/src/scripts/test_one_endpoint.py)