# Guía de instalación Windows Server 2022 y 2019

## Pre-instalación

1. Se descarga la imagen ISO de Windows Server [2019](https://www.microsoft.com/es-es/evalcenter/download-windows-server-2019) o [2022](https://www.microsoft.com/es-es/evalcenter/download-windows-server-2022)

2. Se [crea un escritorio a partir de ese media](../../../advanced/media.es/#generar-escritorio-a-partir-de-media) con el siguiente hardware virtual:

    - Visores: **SPICE**
    - vCPUS: **4**
    - Memoria (GB): **8**
    - Videos: **Default**
    - Boot: **CD/DVD**
    - Disk Bus: **Default**
    - Tamaño del disco (GB): **100**
    - Redes: **Default**
    - Plantilla: **Microsoft windows 10 with Virtio devices UEFI**

3. Se [edita el escritorio y se asignan más media](../../../user/edit_desktop.es/#media), de tal forma que acabe teniendo los siguientes:

    - **WinServer2022 - ES** (instalador)
    - **virtio-win-X** (drivers)
    - **Optimization Tools** (software de optimización para S.O. Windows)


## Instalación

1. Se **envia "Ctr+Alt+Supr"** y se pulsa cualquier tecla del teclado en la segunda pantalla

    ![](install.es.images/pantalla1.png){width="45%"}
    ![](install.es.images/send_key.png){width="13%"}
    ![](install.es.images/pantalla2.png){width="45%"}

2. Tipo de instalación

    ![](install.es.images/1-es.png){width="45%"}
    ![](install.es.images/2-es.png){width="45%"}

    ![](install.es.images/3-es.png){width="45%"}
    ![](install.es.images/4-es.png){width="45%"}

3. Cargar drivers de sistema operativo 

    ![](install.es.images/5-es.png){width="45%"}
    ![](install.es.images/6-es.png){width="45%"}

    ![](install.es.images/7-es.png){width="45%"}
    ![](install.es.images/8-es.png){width="45%"}

    ![](install.es.images/9-es.png){width="45%"}
    ![](install.es.images/10-es.png){width="45%"}

    ![](install.es.images/11-es.png){width="45%"}
    ![](install.es.images/12-es.png){width="45%"}


## Configuración

1. Se apaga y [edita el escritorio](../../../user/edit_desktop.es) con el siguiente hardware virtual

    - Visores: **RDP** y **RDP en el navegador**
    - Login RDP:
        - Usuario: **Administrador**
        - Contraseña: **Aneto_3404**
    - vCPUS: **4**
    - Memoria (GB): **8**
    - Videos: **Default**
    - Boot: **Hard Disk**
    - Disk Bus: **Default**
    - Redes: **Default** y **Wireguard VPN**

2. Se arranca el escritorio, tardará unos segundos en iniciar el sistema operativo

    ![](install.es.images/13-es.png){width="45%"}

3. Se asigna la contraseña **Aneto_3404** al usuario **Administrador**

    ![](install.es.images/14-es.png){width="45%"}
    ![](install.es.images/15-es.png){width="45%"}

4. Se instalan los dos drivers **virtio** del media del escritorio. Ambas instalaciones son rápidas y sólo hay que darle al botón **Next**

    ![](install.es.images/16-es.png){width="45%"}
    ![](install.es.images/17-es.png){width="45%"}

    ![](install.es.images/18-es.png){width="45%"}
    ![](install.es.images/19-es.png){width="45%"}

5. Se habilitan las conexiones por **Escritorio Remoto**. Se habilita la primera casilla, y se deshabilita la segunda casilla en **Configuración avanzada**

    ![](install.es.images/20-es.png){width="20%"}
    ![](install.es.images/21-es.png){width="38%"}
    ![](install.es.images/22-es.png){width="39%"}

6. Se comprueban las **actualizaciones del sistema**, las cuales van a tardar bastante en descargarse e instalarse, para estar al día con la última versión de Windows Server (seguramente se necesite reiniciar el sistema varias veces en busca de nuevas actualizaciones).

    ![](install.es.images/23-es.png){width="60%"}


## Optimization tools

1. Se abre el ejecutable del media asignado al escritorio

    ![](install.es.images/24-es.png){width="45%"}

2. Se presiona el botón **Analizar** y seguidamente **Opciones comunes**

    ![](install.es.images/25-es.png){width="45%"}
    ![](install.es.images/26-es.png){width="45%"}

3. Se configuran las opciones como en las siguientes imágenes

    ![](install.es.images/27-es.png){width="45%"}
    ![](install.es.images/28-es.png){width="45%"}

    ![](install.es.images/29-es.png){width="45%"}
    ![](install.es.images/30-es.png){width="45%"}

    ![](install.es.images/31-es.png){width="45%"}
    ![](install.es.images/32-es.png){width="45%"}

    ![](install.es.images/33-es.png){width="45%"}
    ![](install.es.images/34-es.png){width="45%"}

4. (**SI EL IDIOMA CATALÁN ESTÁ DEFINIDO, SI NO, OMITIR**) Se filtra por la palabra **idioma** y se desactivan las siguientes 3 opciones:

    ![](../win10/install.es.images/58.png){width="45%"}

5. Se presiona el botón **Optimizar** y se espera a que salga una pantalla como la siguiente con el resumen del resultado

    ![](install.es.images/36-es.png){width="45%"}

6. Se reinicia el sistema
