# Introducción

Para acceder al servidor, es necesario dirigirse al enlace **https://domini_isardvdi**, donde aparecerá la página de inicio de sesión.

![](./local_access.es.images/local_access1.png){width="80%"}

!!! info "La cuenta de usuario puede ser de uno de estos tipo:"
    - **[Local](../login/local_access.es.md)**: Se han proporcionado la **categoría** y **credenciales**.
    - **[OAuth2 (Google/Gitlab)](../login/oauth_access.es.md)**: Se ha proporcionado un **código de acceso** necesario para completar el registro con una cuenta personal.
    - **[SAML](../login/saml_access.es.md)**: Credenciales corporativas para iniciar la sesión. También se ha proporcionado un **código de acceso** para completar el autorregistro.



