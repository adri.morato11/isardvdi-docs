# Upgrade

## From Quickstart

### Manually

**We recommend checking out the new version of isardvdi.cfg.example for any new parameters you want to set.**

Every time an update is made, the build.sh script must be run, which will read the configuration variables from isardvdi.cfg. After doing a build.sh, we download the updated version of the containers and restart the ones that have been modified, IsardVDI should be operational again.

```
cd path_to_isard_source
git pull
./build.sh
docker-compose pull
docker-compose up -d
```

### Automatic

We recommend configuring automatic updates using a cron that runs the following [script](https://gitlab.com/isard/isardvdi/-/blob/main/sysadm/isard-upgrade-cron.sh).

**In case of modifications in the version of isardvdi.cfg.example, the automatic update will not be carried out.**





## From Source/Build

The procedure to upgrade is as follows:

1. Go to your src folder
2. Get new src: ```git pull```
3. Check if there are any new environment vars in isardvdi.cfg.example that you don't have in your current isardvdi.cfg
4. Build yml: ```bash build.sh```
5. Now you can choose between:
  - build your own images: ```docker-compose -f docker-compose.build.yml build```
  - or pull current images: ```docker-compose pull```
6. Bring the new containers up (upgrades will be automated): ```docker-compose up -d```

## Automatic upgrades

There is an script inside isardvdi repo in sysadm folder self explanatory but here you have the main steps to setup in your server:

1. Install jq package if not already in your distro. In debian: ```àpt install jq```
2. Setup the cron in /etc/cron.d/isardvdi-upgrades based on the comments inside the script. For example: