# Client-Server

This guide will explain in a user manual way the necessary steps to create connectivity between teacher desktop and a student's desktop in order to supervise the classroom work in real time, on the Isard VDI virtualization environment.

The first point, documents how to establish a connection between a teacher's desktop and a student's, thus forming a client-server structure. Both roles will connect through the same internal network with which the client can access the server's work area, and viceversa.

An environment as real as possible will be created to adjust the guide to practice. Users and desktops will be created to work with. If the environment with desktops, templates and users is already created, only the [steps that remain](#create-and-share-network-interface) to be carried out as administrator, teacher and student must be followed.

## As admin user

### Create users and groups

To begin, some test users are created with whom to carry out the practice. The most recommended is that the groups to which they will belong are already created before creating the users.

To create a group, go to the Administration panel and in the "Users" section.

In the section of "Groups", press the button ![](./client_server.images/client_server1.png)

![](./client_server.images/client_server2.png)

And the form is filled in, in our case for the example we have created a group called "classroom_100".

![](./client_server.images/client_server3.png)

The Administrator user creates an Advanced role teacher user, a classroom and four Users role students with this information:

![](./client_server.images/client_server4.png)

![](./client_server.images/client_server5.png)

### Create desktops

In this section we proceed to create two respective templates for the teacher and the student that will also be shared respectively. So that users can create desktops to work with based on the modified template that has been prepared for them. Other templates already created in the environment can be used since the network configuration will go on the desktop; this step is carried out if neither the teacher nor the student have templates.

#### Teacher's desktop

Create a debian_profe template from, for example, a Debian 9.5.0 desktop, where it is shared with the teacher test_docent01 and the Default and Internal network interfaces are assigned:

![](./client_server.images/client_server10.png)


#### Student's desktop

Finally, create the ubuntu_alum template from, for example, an Ubuntu 20.04 desktop, which is shared with the teacher test_docent01 and with the group classroom_100 and add the internal network interface:

![](./client_server.images/client_server11.png)

### Create desktops

Login with the teacher credentials test_profe01 and create two desktops based on the shared template with which the connection test will be carried out. A desktop for the teacher and another for the students, created with their respective templates, which are named web_server (teacher) and web_client (student)

The creation of desktops based on templates is explained in the [templates](/advanced/templates/#create) advanced section of the manual.

**Newly created desktops will have the same hardware parameters as the template used to create the desktop with; You can change the template options by editing it at any time, but changes will only apply to new desktops created from this template from now on. Old desktops already created from this template will keep old template settings, unless also edited.**

![](./client_server.images/client_server1-1.png) 

### Create and share Network Interface

In order for the students and the teacher to be connected through an internal network and to establish connectivity through that network, the student and teacher desktops are connected with the same network interface. This should be done by an administrator.

As the teacher's desktop and the student desktop belong to different users, the way to create the new network is by using the **ovs** network. Select that and assign a VLAN id in the range shown and a name to this new network.

The administrator goes to the Administration panel and in the "Domains" dropdown, press the button "Resources".

![](./client_server.images/client_server6.png)

This section shows a list of all the network interfaces already created in the system. There exists different [system interfaces](/user/networks/).

![](./client_server.images/client_server7.png)


For users to use networks, they have to be shared. The first interface, "Default", is already shared by default with all users in the system, so only the newly created interface, "Lab", needs to be shared.

Press the button ![](./client_server.images/client_server8.png) and a dialog box will appear with a form to fill out.

Assigning the group "classroom_100", will share with all the users of that same group. All the desktops in this group that attach this new interface will have connection between them (VLAN):

![](./client_server.images/client_server9.png)


## Network setup in desktops

Although there are many, here are two ways to configure desktop networks: one using the graphical interface and another using the command line or terminal.

Once created, start them both and access the desktops through the viewers.

![](./client_server.images/client_server12.png) 

### Server Settings

An IP address is automatically offered on the first servidor_web *default* network interface through the DHCP service of the external network, so the machine must have Internet access by default.

To configure the second interface with the internal network, Assign an IP address manually.

From now on:

#### Graphical interface

In Settings, press the button ![](./client_server.images/client_server13.png), and in the IPv4 section, configure the network this way:

![](./client_server.ca.images/client_server14.png)

![](./client_server.ca.images/client_server15.png)

![](./client_server.ca.images/client_server16.png)

![](./client_server.ca.images/client_server17.png)

It can be easily checked by opening a terminal and typing the command

``` 
ip -c a 
```

It returns an output similar to this:

![](./client_server.images/client_server18.png)


#### Command Line

To do it with the terminal, use the network-manager package, which must be installed on the machine, and using the nmcli software and the following commands, configure the second interface, where the interface_name value corresponds to the network card that we want to modify.

If the name is not clear, you can check with the following command:


```
sudo nmcli connection show
```

![](./client_server.images/client_server19.png)

```
sudo nmcli connection down interface_name
sudo nmcli connection modify interface_name ipv4.method manual ipv4.address 192.168.200.10/24
sudo nmcli connection up interface_name
```

With the same command we check the modification of the interface:

```
ip -c a
```

![](./client_server.images/client_server20.png)


### Client Configuration

cliente_web is then assigned an IP address for the internal network, like this for Ubuntu:

#### Graphic interface

In the Wired Network Settings section:

![](./client_server.es.images/client_server21.png)

![](./client_server.es.images/client_server22.png)

![](./client_server.es.images/client_server23.png)

With the same command the verification is done:

![](./client_server.images/client_server24.png)


#### Command line

The value interface_name corresponds to the network card that we want to modify.

If the name is not clear, with the following command you can check:


```
sudo nmcli connection show
```
![](./client_server.images/client_server25.png)

Open a terminal and through the "network-manager" package, configure the interface:

```
sudo nmcli connection down interface_name
sudo nmcli connection modify interface_name ipv4.method manual ipv4.addresses 192.168.200.11/24
sudo nmcli connection up interface_name
```

With the same command we check the modification of the interface:

```
ip -c a
```

![](./client_server.images/client_server26.png)


### Connectivity test

#### ‘Ping’ Command

To check the correct communication between desktops, with the ping command, a packet is sent to the IP address or domain described to check a connection between both machines. The command is carried out on both machines and thus returns a response like this, verifying that the connection exists and they communicate:

![](./client_server.images/client_server27.png)


#### Web environment with 'Apache'

Finally, you can also test using the Apache HTTP server that the connection is established.
It is installed via terminal on the server_web desktop and cliente_web connects to the software via the machine's web browser.

![](./client_server.ca.images/client_server28.png)

The Apache installation is verified and the existence of the verification HTML file that the service shows when you connect to it is displayed:

![](./client_server.ca.images/client_server29.png)

The client_web just has to open a web browser and type the IP address of the server machine for Apache to respond:

![](./client_server.images/client_server30.png)


Please, note that some OS come with an already active firewall that could be blocking connections. Disable it in your lab virtual desktops to solve it.